+++
author = "Göran Svensson"
date = "2016-04-06T04:34:49Z"
description = "Now you can sign in with Twitter, Facebook and GitHub"
draft = false
keywords = ["Twitter", "Facebook", "Sign in"]
tags = ["Appernetic Static Site Generator", "Appernetic Service", "Enhancements"]
title = "Now you can sign in with twitter facebook and github"
topics = ["topic 1"]
type = "post"

+++
![Appernetic Static Site Generator with Twitter, Facebook and GitHub sign in!][1]
We have just released a new version! Now you can sign in with Twitter, Facebook and GitHub.  Enjoy folks!
https://appernetic.io #contentmanagement

  [1]: https://res.cloudinary.com/appernetic/v1459917514/xffcn0sgfvrk6xbawhep
